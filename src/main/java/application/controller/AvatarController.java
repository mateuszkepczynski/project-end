package application.controller;

import application.model.AvatarDto;
import application.model.User;
import application.repository.UserRepository;
import application.service.CookieService;
import application.service.UserAvatarService;


import application.service.UserService;
import application.service.UserServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;


@RestController
public class AvatarController {

    @Autowired
    UserAvatarService userAvatarService;
    @Autowired
    UserService userService;
    @Autowired
    CookieService cookieService;

    @RequestMapping(value = "/user/{ID}/avatar", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation(value = "Dodaje avatar userowi.", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Avatar dodany do konta użytkownika", response = Void.class),
            @ApiResponse(code = 204, message = "Problem z dodaniem avatara", response = Void.class),
            @ApiResponse(code = 404, message = "Problem ze znalezieniem pliku lub usera", response = Void.class)})
    public ResponseEntity<String> addAvatar(@ApiParam(value = "Avatar") @RequestPart Optional<MultipartFile> file,HttpServletRequest request) {

        Optional<String> optionalCookieValue = cookieService.getCookieValue(request);
        if(!optionalCookieValue.isPresent()){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        String cookieValue = optionalCookieValue.get();
        String token = cookieService.getTokenFromCookieValue(cookieValue);
        Long userid = Long.parseLong(token);

        if(!userService.getLoggedUser().containsKey(cookieValue)){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        if (!userService.userIsPresent(userid)) {
            return new ResponseEntity<>("Brak usera o podanym id", HttpStatus.NOT_FOUND);
        }

        if (!file.isPresent()) {
            return new ResponseEntity<>("Brak pliku", HttpStatus.NOT_FOUND);
        }

        MultipartFile avatar = file.get();

        if (!avatar.getContentType().startsWith("image")) {
            return new ResponseEntity<>("zły typ pliku", HttpStatus.BAD_REQUEST);
        }

        AvatarDto avatarDto = getAvatarDto(userid, avatar);

        if (userAvatarService.addAvatar(avatarDto)) {
            return new ResponseEntity<>("Dodano avatar", HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private AvatarDto getAvatarDto(Long userid, MultipartFile avatar) {
        AvatarDto avatarDto = new AvatarDto();
        avatarDto.setFileName(avatar.getOriginalFilename());
        avatarDto.setFileType(avatar.getContentType());
        avatarDto.setUserid(userid);
        avatarDto.setAvatar(avatar);
        return avatarDto;
    }

    @RequestMapping(value = "/avatar/{ID}", method = RequestMethod.GET)
    public ResponseEntity<String> getFile(HttpServletResponse response, HttpServletRequest request, @PathVariable(value = "ID")  Long avatarid) {

        Optional<String> optionalCookieValue = cookieService.getCookieValue(request);
        if(!optionalCookieValue.isPresent()){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        String cookieValue = optionalCookieValue.get();
        String token = cookieService.getTokenFromCookieValue(cookieValue);
        Long userid = Long.parseLong(token);

        if(!userService.getLoggedUser().containsKey(cookieValue)){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        User userFromFile = userAvatarService.getUserAvatar(avatarid).getUser();
        User userFromCookie = userService.getUser(userid);

        if(!userFromCookie.equals(userFromFile)){
            return new ResponseEntity<>("Nie masz prawa do pliku", HttpStatus.UNAUTHORIZED);
        }

        if(!userAvatarService.avatarIsPresent(avatarid)){
            return new ResponseEntity<>("Brak pliku", HttpStatus.NOT_FOUND);
        }

        try {
            Optional<MultipartFile> avatar = userAvatarService.getAvatar(avatarid);

            if (avatar.isPresent()) {
                MultipartFile multipartFile = avatar.get();
                InputStream is = multipartFile.getInputStream();
                response.setContentType(multipartFile.getContentType());
                response.setHeader("Content-Disposition", "attachment; filename=" + multipartFile.getOriginalFilename().replace("–","-"));
                IOUtils.copy(is, response.getOutputStream());
                response.flushBuffer();
                is.close();
            }
        } catch (IOException ex) {
            throw new RuntimeException("IOError writing file to output stream");
        }
        return new ResponseEntity<>("wysyłam plik", HttpStatus.OK);
    }

    @RequestMapping(value = "/avatar/{ID}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Usuwa plik avatara.", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Plik ostał usunięty", response = Void.class),
            @ApiResponse(code = 404, message = "Brak pliku do usunięcia", response = Void.class)})
    public ResponseEntity<String> removeAvatar(HttpServletRequest request,@PathVariable(value = "ID") Long avatarid) {

        Optional<String> optionalCookieValue = cookieService.getCookieValue(request);
        if(!optionalCookieValue.isPresent()){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        String cookieValue = optionalCookieValue.get();
        String token = cookieService.getTokenFromCookieValue(cookieValue);
        Long userid = Long.parseLong(token);

        if(!userService.getLoggedUser().containsKey(cookieValue)){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        User userFromFile = userAvatarService.getUserAvatar(avatarid).getUser();
        User userFromCookie = userService.getUser(userid);

        if(!userFromCookie.equals(userFromFile)){
            return new ResponseEntity<>("Nie masz prawa do pliku", HttpStatus.UNAUTHORIZED);
        }

        if(!userAvatarService.avatarIsPresent(avatarid)){
            return new ResponseEntity<>("Brak pliku", HttpStatus.NOT_FOUND);
        }

        if (userAvatarService.removeFile(avatarid)) {
            return new ResponseEntity<>("Usunięto plik", HttpStatus.OK);
        }

        return new ResponseEntity<>("Plik nie został znaleziony", HttpStatus.NOT_FOUND);
    }
}
