package application.controller;

import application.mappers.FileMapper;
import application.model.FileDao;
import application.model.FileDto;
import application.model.User;
import application.model.UserFile;
import application.repository.UserFileRepository;
import application.service.CookieService;
import application.service.UserFileService;
import application.service.UserService;
import io.swagger.annotations.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@RestController
public class FileController {
    @Autowired
    UserFileService userFileService;
    @Autowired
    UserService userService;
    @Autowired
    FileMapper fileMapper;
    @Autowired
    UserFileRepository userFileRepository;
    @Autowired
    CookieService cookieService;

    @RequestMapping(value = "/user/{ID}/file", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation(value = "Dodaje plik usera.", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Plik dodany do konta użytkownika", response = Void.class),
            @ApiResponse(code = 204, message = "Problem z dodaniem pliku", response = Void.class),
            @ApiResponse(code = 404, message = "Problem ze znalezieniem pliku lub usera", response = Void.class),
            @ApiResponse(code = 401, message = "Użytkownik nie jest zalogowany", response = Void.class)})
    public ResponseEntity<String> addFile(@ApiParam(value = "Plik") @RequestPart Optional<MultipartFile> file, HttpServletRequest request) {


        Optional<String> optionalCookieValue = cookieService.getCookieValue(request);
        if(!optionalCookieValue.isPresent()){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        String cookieValue = optionalCookieValue.get();
        String token = cookieService.getTokenFromCookieValue(cookieValue);
        Long userid = Long.parseLong(token);

        if(!userService.getLoggedUser().containsKey(cookieValue)){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        if (!file.isPresent()) {
            return new ResponseEntity<>("Brak pliku", HttpStatus.NOT_FOUND);
        }

        MultipartFile multipartFile = file.get();

        FileDto fileDto = fileMapper.map(multipartFile, userid);

        if (userFileService.addFile(fileDto)) {
            return new ResponseEntity<>("Dodano plik", HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @RequestMapping(value = "/file/{ID}", method = RequestMethod.GET)
    public ResponseEntity<String> getFile(HttpServletResponse response, HttpServletRequest request, @PathVariable(value = "ID") Long fileid) {

        Optional<String> optionalCookieValue = cookieService.getCookieValue(request);
        if(!optionalCookieValue.isPresent()){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        String cookieValue = optionalCookieValue.get();
        String token = cookieService.getTokenFromCookieValue(cookieValue);
        Long userid = Long.parseLong(token);

        if(!userService.getLoggedUser().containsKey(cookieValue)){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        User userFromFile = userFileService.getUserFile(fileid).getUser();
        User userFromCookie = userService.getUser(userid);

        if(!userFromCookie.equals(userFromFile)){
            return new ResponseEntity<>("Nie masz prawa do pliku", HttpStatus.UNAUTHORIZED);
        }

        if(!userFileService.fileIsPresent(fileid)){
            return new ResponseEntity<>("Brak pliku", HttpStatus.NOT_FOUND);
        }

        InputStream is = null;
        try {
            String filePath = userFileRepository.getOne(fileid).getFilePath();
            is = new FileInputStream(filePath);
            UserFile userFile = userFileRepository.getOne(fileid);
            response.setContentType(userFile.getFileType());
            response.setHeader("Content-Disposition", "attachment; filename=" + userFile.getOriginalFileName());
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
            is.close();
        } catch (IOException ex) {
            throw new RuntimeException("IOError writing file to output stream");
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return new ResponseEntity<>("wysyłam plik", HttpStatus.OK);
    }


    @RequestMapping(value = "/file/{ID}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Usuwa plik.", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Plik ostał usunięty", response = Void.class),
            @ApiResponse(code = 404, message = "Brak pliku do usunięcia", response = Void.class)})
    public ResponseEntity<String> removeFile(HttpServletRequest request,@PathVariable(value = "ID") Long fileid) {

        Optional<String> optionalCookieValue = cookieService.getCookieValue(request);
        if(!optionalCookieValue.isPresent()){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        String cookieValue = optionalCookieValue.get();
        String token = cookieService.getTokenFromCookieValue(cookieValue);
        Long userid = Long.parseLong(token);

        if(!userService.getLoggedUser().containsKey(cookieValue)){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        User userFromFile = userFileService.getUserFile(fileid).getUser();
        User userFromCookie = userService.getUser(userid);

        if(!userFromCookie.equals(userFromFile)){
            return new ResponseEntity<>("Nie masz prawa do pliku", HttpStatus.UNAUTHORIZED);
        }
        if(!userFileService.fileIsPresent(fileid)){
            return new ResponseEntity<>("Brak pliku", HttpStatus.NOT_FOUND);
        }

        if (userFileService.removeFile(fileid)) {
            return new ResponseEntity<>("Usunięto plik", HttpStatus.OK);
        }

        return new ResponseEntity<>("Plik nie został znaleziony", HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/file", method = RequestMethod.GET)
    @ApiOperation(value = "Pobiera liste wszystkich plikow.", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Pliki zostaly wyświetlone", response = Void.class),
            @ApiResponse(code = 404, message = "Brak pliku do usunięcia", response = Void.class)})
    public ResponseEntity<List> getFiles(HttpServletRequest request) {

        Optional<String> optionalCookieValue = cookieService.getCookieValue(request);
        if(!optionalCookieValue.isPresent()){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        String cookieValue = optionalCookieValue.get();
        String token = cookieService.getTokenFromCookieValue(cookieValue);
        Long userid = Long.parseLong(token);

        if(!userService.getLoggedUser().containsKey(cookieValue)){
            return new ResponseEntity<>( HttpStatus.UNAUTHORIZED);
        }

        List<FileDao> userFile = userFileService.getUserFileList(userid);

        if (userFile.isEmpty()) {
            return new ResponseEntity<>(userFile, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(userFile, HttpStatus.OK);
    }

}
