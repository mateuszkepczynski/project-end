package application.controller;


import application.mappers.UserMapper;
import application.model.LoginDto;
import application.model.User;
import application.model.UserDto;

import application.service.CookieService;
import application.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CookieService cookieService;

    @RequestMapping(value = "/user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Rejestruje usera.", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "User zarejestrowany", response = Void.class),
            @ApiResponse(code = 400, message = "Niepoprawne dane wejściowe", response = Void.class)})
    public ResponseEntity<String> addUser(HttpServletRequest request, HttpServletResponse response, @ApiParam(value = "Rejestrowany user") @RequestBody UserDto userDto) {

        if (userService.findUser(userDto.getUsername()).isPresent()) {
            return new ResponseEntity<String>("Username jest juz w bazie", HttpStatus.CONFLICT);
        }

        userService.register(userMapper.map(userDto));

        return new ResponseEntity<String>("Ok", HttpStatus.OK);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Loguje usera.", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "User zalogowany", response = Void.class),
            @ApiResponse(code = 401, message = "Logowanie nieudane", response = Void.class)})
    public ResponseEntity<String> loginProcess(HttpServletRequest request, HttpServletResponse response, @ApiParam(value = "Dane logowania") @RequestBody LoginDto login) {

        Optional<String> optionalToken = userService.loginUser(login);
        if (optionalToken.isPresent()) {

            Cookie cookie = new Cookie("logU", optionalToken.get());
            response.addCookie(cookie);

            return new ResponseEntity<>("Zalogowano poprawnie ", HttpStatus.OK);
        }

        return new ResponseEntity<>("Błędne dane logowania", HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ApiOperation(value = "Wylogowuje usera.", notes = "", response = Void.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "User wylogowany", response = Void.class),
            @ApiResponse(code = 401, message = "wylogowywanie nieudane", response = Void.class)})
    public ResponseEntity<String> logout(HttpServletRequest request) {

        Optional<String> optionalCookieValue = cookieService.getCookieValue(request);
        if (!optionalCookieValue.isPresent()) {
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        String cookieValue = optionalCookieValue.get();
        String token = cookieService.getTokenFromCookieValue(cookieValue);
        Long userid = Long.parseLong(token);

        if(!userService.getLoggedUser().containsKey(cookieValue)){
            return new ResponseEntity<>("Użytkownik nie jest zalogowany", HttpStatus.UNAUTHORIZED);
        }

        if (userService.logoutUser(cookieValue)) {
            return new ResponseEntity<>("Wylogowano poprawnie ", HttpStatus.OK);
        }

        return new ResponseEntity<>("Problem z wylogowaniem użytkownika", HttpStatus.CONFLICT);
    }


}
