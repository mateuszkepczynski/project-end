package application.mappers;

import application.model.FileDao;
import application.model.FileDto;
import application.model.UserFile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileMapper {

    public FileDto map(MultipartFile multipartFile, Long userid) {
        FileDto fileDto = new FileDto();
        fileDto.setFileName(multipartFile.getOriginalFilename());
        fileDto.setFileType(multipartFile.getContentType());
        fileDto.setUserid(userid);
        fileDto.setFile(multipartFile);
        return fileDto;
    }

    public FileDao map(UserFile userFile) {
        FileDao fileDao = new FileDao();
        fileDao.setId(userFile.getId());
        fileDao.setFileName(userFile.getOriginalFileName());
        fileDao.setFileType(userFile.getFileType());

        return fileDao;
    }
}
