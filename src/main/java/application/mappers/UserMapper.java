package application.mappers;

import application.model.User;
import application.model.UserDto;
import com.sun.imageio.plugins.jpeg.JPEG;
import org.springframework.stereotype.Component;

import java.sql.Blob;

@Component
public class UserMapper {

    public User map(UserDto user) {
        User entity = new User();
        entity.setEmail(user.getEmail());
        entity.setFirstname(user.getFirstname());
        entity.setLastname(user.getLastname());
        entity.setPassword(user.getPassword());
        entity.setEncryptPassword(user.getEncryptPassword());
        entity.setUsername(user.getUsername());
        return entity;
    }

    public UserDto map(User entity) {
        UserDto user = new UserDto();
        user.setEmail(entity.getEmail());
        user.setFirstname(entity.getFirstname());
        user.setLastname(entity.getLastname());
        user.setPassword(entity.getPassword());
        user.setEncryptPassword(entity.getEncryptPassword());
        user.setUsername(entity.getUsername());
        return user;
    }

}
