package application.service;

import application.model.LoginDto;
import application.model.User;

import java.util.Map;
import java.util.Optional;

public interface UserService {

    void register(User user);

    Optional<String> loginUser(LoginDto login);

    Optional<User> findUser(String username);

    boolean userIsPresent(Long id);

    User getUser(Long id);

    Map<String, User> getLoggedUser();

    boolean checkIfUserIsActualLogged(String token);

    boolean logoutUser(String key);
}
