package application.service;

import application.mappers.FileMapper;
import application.model.FileDao;
import application.model.FileDto;
import application.model.User;
import application.model.UserFile;
import application.repository.UserFileRepository;
import application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Service
public class UserFileService {

    @Autowired
    private UserFileRepository userFileRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FileMapper fileMapper;

    @Transactional
    public boolean addFile(FileDto fileDto) {
        MultipartFile file = fileDto.getFile();
        User user = userRepository.findOne(fileDto.getUserid());
        String filePath = "E:\\kepadrive\\" + user.getUsername() + "\\" + String.valueOf(LocalDate.now());
        new File(filePath).mkdir();
        String fileName = String.valueOf(System.currentTimeMillis());
        String filePathWithFileName = filePath + "\\" + fileName;
        UserFile userFile = addUserFile(fileDto, filePathWithFileName, user, fileName);
        userFileRepository.save(userFile);
        addFileToUserFiles(user, userFile);

        DataInputStream strumieńWejściowy = null;
        DataOutputStream strumieńWyjściowy = null;

        try {
            strumieńWejściowy = new DataInputStream(file.getInputStream());
            strumieńWyjściowy = new DataOutputStream(new FileOutputStream(filePathWithFileName));
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono takiego pliku");
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        int ilośćSczytanychBajtów = 0;
        int całkowitaIlośćBajtów = 0;
        byte[] bufor = new byte[1024];

        try {
            while ((ilośćSczytanychBajtów = strumieńWejściowy.read(bufor)) != -1) {
                strumieńWyjściowy.write(bufor, 0, ilośćSczytanychBajtów);
                całkowitaIlośćBajtów += ilośćSczytanychBajtów;
            }
        } catch (IOException e) {
            System.out.println("Bład wejścia-wyjścia");
            return false;
        }

        try {
            if(strumieńWejściowy != null) strumieńWejściowy.close();
            if(strumieńWyjściowy != null) strumieńWyjściowy.close();
        } catch (IOException e) {
            System.out.println("Błąd zamykania strumieni");
            return false;
        }

        return true;
    }

    private void addFileToUserFiles(User user, UserFile userFile) {
        user.getFiles().put(userFile.getId(), userFile);
        userRepository.save(user);
    }

    private UserFile addUserFile(FileDto fileDto, String filePath, User user,String fileName) {
        UserFile userFile = new UserFile();
        userFile.setFilePath(filePath);
        userFile.setFileName(fileName);
        userFile.setFileType(fileDto.getFileType());
        userFile.setOriginalFileName(fileDto.getFileName());
        userFile.setUser(user);
        return userFile;
    }

    @Transactional
    public boolean removeFile(Long fileid) {

        if (fileIsPresent(fileid)) {
            UserFile byid = userFileRepository.findByid(fileid);

            String filePath = byid.getFilePath();
            if (!deleteFile(filePath)) {
                return false;
            }

            removeFileFromUserFiles(fileid, byid);

            userFileRepository.delete(fileid);
            return true;
        }
        return false;
    }

    public UserFile getUserFile(Long id){
        return userFileRepository.getOne(id);
    }

    private void removeFileFromUserFiles(Long fileid, UserFile byid) {
        User user = byid.getUser();
        user.getFiles().remove(fileid);
        userRepository.save(user);
    }

    public boolean fileIsPresent(Long id) {
        if (userFileRepository.exists(id)) {
            return true;
        }
        return false;
    }

    public boolean deleteFile(String path) {
        try {

            File file = new File(path);

            if (file.delete()) {
                return true;
            }

        } catch (Exception e) {

            e.printStackTrace();

        }
        return false;
    }

    public List<FileDao> getUserFileList(Long userid) {
        User user = userRepository.getOne(userid);
        List<FileDao> fileDaos = new ArrayList<>();
        for (UserFile userFile : user.getFiles().values()) {
            fileDaos.add(fileMapper.map(userFile));
        }
        return fileDaos;
    }

    private class  Encypter extends InputStream {

        private final InputStream inputStream;

        private Encypter(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public int read() throws IOException {
            return inputStream.read() ^ 7;
        }
    }

}

