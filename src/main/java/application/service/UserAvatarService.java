package application.service;

import application.model.AvatarDto;
import application.model.User;
import application.model.UserAvatar;
import application.repository.UserAvatarRepository;
import application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.util.Optional;

@Service
public class UserAvatarService {

    @Autowired
    private UserAvatarRepository userAvatarRepository;
    @Autowired
    private UserRepository userRepository;

    public boolean addAvatar(AvatarDto avatarDto) {
        try {
            byte[] avatarByte = avatarDto.getAvatar().getBytes();
            User user = userRepository.findOne(avatarDto.getUserid());
            UserAvatar userAvatar = new UserAvatar();
            userAvatar.setAvatar(avatarByte);
            userAvatar.setFileName(avatarDto.getFileName());
            userAvatar.setFileType(avatarDto.getFileType());
            userAvatar.setUser(user);
            userAvatarRepository.save(userAvatar);
            user.setDefAvt(userAvatar.getId());
            userRepository.save(user);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Transactional
    public boolean removeFile(Long avatarId) {

        if (avatarIsPresent(avatarId)) {
            UserAvatar byid = userAvatarRepository.findByid(avatarId);
            User user = byid.getUser();
            user.getAvatars().remove(avatarId);
            if(user.getDefAvt().equals(avatarId)){
                user.setDefAvt(null);
            }
            userRepository.save(user);
            userAvatarRepository.delete(avatarId);
            return true;
        }
        return false;
    }

    public boolean avatarIsPresent(Long id) {
        if (userAvatarRepository.exists(id)) {
            return true;
        }
        return false;
    }

    public Optional<MultipartFile> getAvatar(Long avatarid) {
        if(userAvatarRepository.exists(avatarid)) {
            UserAvatar userAvatar = userAvatarRepository.getOne(avatarid);

            byte[] file = userAvatar.getAvatar();
            MultipartFile multipartFile = new MockMultipartFile(userAvatar.getFileName(), userAvatar.getFileName(), userAvatar.getFileType(), file);
            return Optional.of(multipartFile);
        }
        return Optional.empty();
    }

    public UserAvatar getUserAvatar(Long userAvatarId){
        return userAvatarRepository.getOne(userAvatarId);
    }
}
