package application.service;

import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
public class CookieService {

    public Optional<String> getCookieValue(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return Optional.empty();
        }
        String cookieValue = null;
        for (Cookie c : cookies) {
            if (c.getName().equals("logU")) {
                cookieValue = c.getValue();
                String[] tokenArray = cookieValue.split("_");
            }
        }
        return Optional.of(cookieValue);
    }

    public String getTokenFromCookieValue(String cookieValue) {
        String[] tokenArray = cookieValue.split("_");
        String token = tokenArray[3];
        return token;
    }


}
