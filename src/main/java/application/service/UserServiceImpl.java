package application.service;

import application.model.User;

import application.model.LoginDto;
import application.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;
import java.io.File;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    private final Map<String, User> loggedUser = new HashMap<>();

    @Transactional
    @Override
    public void register(User user) {
        userRepository.save(user);
        if (!createFolder(user)) {
            System.out.println("problem z utworzeniem folderu dla Usera " + user.getUsername());
        } else {
            System.out.println("folder zostal utworzony");
        }
    }

    @Override
    public Map<String, User> getLoggedUser() {
        return loggedUser;
    }

    private boolean createFolder(User user) {
        String filepath = "E:\\kepadrive\\" + user.getUsername();
        boolean file = new File(filepath).mkdir();
        return file;
    }

    @Override
    public Optional<String> loginUser(LoginDto login) {
        Optional<User> user = findUser(login.getUsername());
        if (user.isPresent()) {
            User userRep = userRepository.findByUsername(login.getUsername());
            if (userRep.getPassword().equals(login.getPassword())) {
                String token = generateToken(userRep);
                loggedUser.put(token, userRep);
                return Optional.of(token);
            }
        }
        return Optional.empty();
    }

    private String generateToken(User user) {
        return user.getUsername().hashCode() + "_" + user.getPassword() + "_" + String.valueOf(System.currentTimeMillis()) + "_" + user.getId();
    }


    @Override
    public Optional<User> findUser(String username) {
        boolean present = false;
        for (User user : userRepository.findAll()) {
            if (user.getUsername().equals(username)) {
                present = true;
            }
        }

        if (present) {
            return Optional.of(userRepository.findByUsername(username));
        }
        return Optional.empty();
    }

    @Override
    public boolean userIsPresent(Long id) {
        if (userRepository.exists(id)) {
            return true;
        }
        return false;
    }

    @Override
    public User getUser(Long id) {
        return userRepository.getOne(id);
    }

    @Override
    public boolean checkIfUserIsActualLogged(String token) {

        if (loggedUser == null) {
            System.out.println("nikt się jeszcze nie zalogował do systemu :(");
        } else {
            System.out.println("Mamy zalogowanego uzytkownika :)");
            User user = loggedUser.get(token);
            System.out.println(loggedUser.get(token));
        }

        for (Map.Entry<String, User> entry : loggedUser.entrySet()) {

            if (entry.getKey().equals(token)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean logoutUser(String key) {

        loggedUser.remove(key);

        return true;
    }


}
