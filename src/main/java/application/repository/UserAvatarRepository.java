package application.repository;

import application.model.User;
import application.model.UserAvatar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAvatarRepository extends JpaRepository<UserAvatar,Long> {
    UserAvatar findByUserId(Long id);
    UserAvatar findByid(Long id);
}
