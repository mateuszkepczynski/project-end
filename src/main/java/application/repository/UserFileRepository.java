package application.repository;

import application.model.UserFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserFileRepository extends JpaRepository<UserFile,Long> {
    UserFile findByFileName(String name);
    UserFile findByid(Long id);

}
