package application.model;

import com.sun.imageio.plugins.jpeg.JPEG;
import javax.persistence.*;
import java.sql.Blob;
import java.util.Map;

import static javax.persistence.FetchType.LAZY;

@Entity
public class User {
    @Id
    @GeneratedValue
    private Long id;

    private String username;
    private String password;
    private String encryptPassword;
    private String firstname;
    private String lastname;
    private String email;
    private Long defAvt;

    @OneToMany
    @MapKey(name="id")
    private Map<Long,UserAvatar> avatars;

    @OneToMany
    @MapKey(name="id")
    private Map<Long,UserFile> files;

    public Long getId() {
        return id;
    }

    public Long getDefAvt() {
        return defAvt;
    }

    public void setDefAvt(Long defAvt) {
        this.defAvt = defAvt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncryptPassword() {
        return encryptPassword;
    }

    public void setEncryptPassword(String encryptPassword) {
        this.encryptPassword = encryptPassword;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<Long, UserAvatar> getAvatars() {
        return avatars;
    }

    public void setAvatars(Map<Long, UserAvatar> avatars) {
        this.avatars = avatars;
    }

    public Map<Long, UserFile> getFiles() {
        return files;
    }

    public void setFiles(Map<Long, UserFile> files) {
        this.files = files;
    }

}
