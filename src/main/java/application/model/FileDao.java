package application.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import org.springframework.web.multipart.MultipartFile;
@ApiModel(description = "Plik")
public class FileDao {
    @ApiModelProperty(value = "ID")
    @JsonProperty("id")
    private Long id;
    @ApiModelProperty(value = "Nazwa Pliku")
    @JsonProperty("fileName")
    private String fileName;
    @ApiModelProperty(value = "Typ Pliku")
    @JsonProperty("fileType")
    private String fileType;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }


}
