package application.model;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;

@Entity
public class UserAvatar {
        @Id
        @GeneratedValue
        private Long id;

        private String fileName;
        private String fileType;

        @Lob
        @Basic(fetch=LAZY)
        private byte[] avatar;

        @ManyToOne
        private User user;

    public Long getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
