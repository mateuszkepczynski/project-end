package application.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.imageio.plugins.jpeg.JPEG;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "Użytkownik")
public class UserDto {

    @ApiModelProperty(value = "Login")
    @JsonProperty("username")
    private String username;
    @ApiModelProperty(value = "Hasło do odszyfrowania zawartości dysku")
    @JsonProperty("encryptPassword")
    private String encryptPassword;
    @ApiModelProperty(value = "Hasło")
    @JsonProperty("password")
    private String password;
    @ApiModelProperty(value = "Imie")
    @JsonProperty("firstname")
    private String firstname;
    @ApiModelProperty(value = "Nazwisko")
    @JsonProperty("lastname")
    private String lastname;
    @ApiModelProperty(value = "e-mail")
    @JsonProperty("email")
    private String email;

    public String getEncryptPassword() {
        return encryptPassword;
    }

    public void setEncryptPassword(String encryptPassword) {
        this.encryptPassword = encryptPassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
