package application.model;

import javax.persistence.*;
import java.sql.Blob;

import static javax.persistence.FetchType.LAZY;

@Entity
public class UserFile {
    @Id
    @GeneratedValue
    private Long id;
    private String fileName;
    private String fileType;
    //private Long parrentId;
    private String filePath;
    private String originalFileName;

    @ManyToOne
    private User user;


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public User getUser() {
        return user;
    }

    public void setUser(application.model.User user) {
        this.user = user;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getId() {
        return id;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

}
