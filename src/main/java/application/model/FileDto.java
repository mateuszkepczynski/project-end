package application.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;

@ApiModel(description = "Plik")
public class FileDto {
    @ApiModelProperty(value = "Nazwa Pliku")
    @JsonProperty("fileName")
    private String fileName;
    @ApiModelProperty(value = "Typ Pliku")
    @JsonProperty("fileType")
    private String fileType;
    @ApiModelProperty(value = "Plik")
    @JsonProperty("file")
    private MultipartFile file;
    @ApiModelProperty(value = "ID Usera")
    @JsonProperty("userid")
    private Long userid;

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
